variable "ec2_region" {

    default = "sa-east-1"  
}

variable "ec2_keypair_name" {

    default = "tf"
}

variable "ec2_instance_type" {
  default = "t2.micro"
}

variable "ec2_image_id" {
  default = "ami-0507655c3acf7dd67"
}
variable "ec2_tags" {
  default = "gitlab-runner"
}

# variable " aws_instance.creating_ec2" {
#  default = "1"
#}


